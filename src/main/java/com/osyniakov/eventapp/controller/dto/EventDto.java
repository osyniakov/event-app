package com.osyniakov.eventapp.controller.dto;

public class EventDto {

    private Long timestamp;
    private Long userId;
    private String event;

    public EventDto(Long timestamp, Long userId, String event) {
        this.timestamp = timestamp;
        this.userId = userId;
        this.event = event;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public Long getUserId() {
        return userId;
    }

    public String getEvent() {
        return event;
    }

}
