package com.osyniakov.eventapp.controller;

import com.osyniakov.eventapp.controller.dto.EventDto;
import com.osyniakov.eventapp.protobuf.MessagesProtos;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

@RestController
@RequestMapping("/events")
public class EventsController {

    @PostMapping
    public void create(@RequestBody EventDto event) throws IOException {
        try (Socket socket = new Socket("localhost", 12301)) {
            OutputStream output = socket.getOutputStream();
            MessagesProtos.Event.newBuilder()
                    .setTimestamp(event.getTimestamp())
                    .setUserId(event.getUserId())
                    .setEvent(event.getEvent())
                    .build()
                    .writeTo(output);
        }
    }

}
