import socket
import messages_pb2

PORT = 12301

sock = socket.socket()
sock.bind(('localhost', PORT))
sock.listen(1)

print 'Listening on port: ' + str(PORT)

while True:

   con, addr = sock.accept()

   data = con.recv(1024)
   if data:
       event = messages_pb2.Event()
       event.ParseFromString(data)
       f = open("events.txt", "a")
       f.write(str(event.timestamp) + ',' + str(event.user_id) + ',' + event.event + '\n')
       f.close()
   else:
      con.close()

sock.close()