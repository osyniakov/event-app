## Description 
Java REST application receives JSON data via HTTP -> 
serializes data using Google Protocol Buffers and sends via TCP socket to Python application -> 
Python application receives data, deserialize and store on Disk

## Prerequisites
1. JDK 8
2. Python >= 2.7 with protobuf.
To install protobuf run: `pip install protobuf`
3. (Optional) Protobuf compiler: `sudo apt install protobuf-compiler`


## How to run
1. Start Python app: `cd src/main/python && python application.py`
2. Start Java app: `./mvnw package && java -jar target/event-app-0.0.1.jar`
3. To verify send POST request using curl:
`curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"timestamp":1518609008, "userId":1123, "event":"2 hours of downtime occured due to the release of version 1.0.5 of the system"}' \
  http://localhost:12300/events`
  
and check file events.txt: `less src/main/python/events.txt`